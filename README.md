A container for the stand-by VESPA resources at Heidelberg

See https://voparis-gitlab.obspm.fr/mdemleitner/mirrorthoughts.git
(in particular the mirror_operator_guide) for docs on how this is
intended to work.
